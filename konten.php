    <!-- Form Section -->
    <section  id="form">
      <div class="container">
        <div class="row text-center">
         <div class="col-lg-8 col-lg-offset-2">
          <h2>Form Pengaduan</h2>
          <form method="POST" action="input.php"  enctype="multipart/form-data">
            <input id="userid" name="userid" type="hidden" value=<?php echo $_SESSION['id']; ?>>
            <input id="username" name="username" type="hidden" value=<?php echo $_SESSION['username']; ?>>
            <br><i class="glyphicon glyphicon-pencil"></i> Laporan : 
            <textarea class="form-control" id="txt_ket" name="keterangan"></textarea>
            <br><i class="fa fa-paperclip"></i> Foto : <br>
            <div id="preview">
              <div class="file" title="Tambah foto">
                <input type="file" name="my_file[]" id="my_file" multiple>
              </div>
            </div>
            <br>
            <br><i class="fa fa-map-marker"></i> Lokasi :     <div id="map"></div><br>
            <input type="text" name='latitude' id='latitude' hidden>
            <input type="text" name='longitude' id='longitude' hidden>
            <input type="text" class='form-control' name='alamat' id='alamat' placeholder="Lokasi">
            <input type="submit" class="btn btn-success" name="kirim" value="Kirim">
          </form>
        </div>
      </div>
    </div>
  </section> 
  <!--End Form Section -->
  <!-- Timeline Section -->
  <section  id="timeline">
    <div class="container">
      <div class="row text-center">
      <h2> Beranda </h2>
       <div class="col-lg-8 col-lg-offset-2" style="text-align: left">
       
         <?php
         $no=1;
         $kueri="SELECT * from pengaduan INNER JOIN user ON pengaduan.id_user = user.id_user and status = '1' order by pengaduan.waktu DESC";
         $query=mysqli_query($konek,$kueri);

         while($row = mysqli_fetch_array($query)){
          ?>

          <div class="panel panel-info">
            <!-- panel head -->
            <div class="panel-heading">
              <h4><?php echo $row['username']; ?></h4>
              <small>
                <?php 
                echo $alamat = ($row['alamat']) ? '<i class="fa fa-map-marker"></i> '.$row['alamat'].'' : '' ; 
                ?>
              </small>
            </div>

            <?php 
            $foto = $row['foto'];
            if ($foto) {
              $arr = explode(', ', $row['foto']);
              if (count($arr) != '1') {
                ?>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner" role="listbox">

                    <?php
                    $no = 1;
                    foreach ($arr as $key => $value) {
                     ?>
                     <div class="item <?php if($no == '1') echo 'active'; ?>">
                      <img src='uploads/<?php echo $value; ?>' class="img-responsive center-block">
                    </div>
                    <?php
                    $no++;
                  } 
                  ?>
                </div>

                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
              <?php  
            } else { 
              foreach ($arr as $key => $value) {
               ?>
               <img src='uploads/<?php echo $value; ?>' class="img-responsive center-block">
               <?php
               $no++;
             } 
           }
         }
         ?>
         <!-- panel body -->
         <div class="panel-body">

           <div class="more">
             <?php echo $row['keterangan']; ?>
           </div><br>

           <div>
             <i class="fa fa-clock-o"></i> <?php echo $row['waktu']; ?>
           </div>
         </div>
       </div>
       <?php 
       $no++;
     } 
     ?>
   </div>
 </div>

</div>
</section> 
<!--End Timeline Section -->

<script>

//MAPS
// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.
var map, infoWindow;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -1.1884589, lng: 114.9390813},
    zoom: 30
  });
  infoWindow = new google.maps.InfoWindow;
  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      document.getElementById('latitude').value = [position.coords.latitude]
      document.getElementById('longitude').value = [position.coords.longitude]

      var latlng = $('#latitude').val() +', '+ $('#longitude').val() ;
      var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlng + "&sensor=false";
      
      $.getJSON(url, function (data) {
        var adress = data.results[0].formatted_address;
        document.getElementById('alamat').value = adress
      });

      infoWindow.setPosition(pos);
      infoWindow.setContent('Lokasi ditemukan.');
      infoWindow.open(map);
      map.setCenter(pos);
    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  $('#map').hide();
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
    'Gagal memuat lokasi anda. Isi lokasi pada kolom input dibawah ini.' :
    'Browser tidak support.');
  infoWindow.open(map);
}      

</script>
<!-- Map Javascript -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-mTET0NPRRzKn43dIEBkMeq6WLJBsVa8&callback=initMap"></script>
</script>