<!--Autentifikasi Section-->
<section  id="autentifikasi">
  <div class="container">
    <div class="row text-center">
      <div class="col-md-8 col-md-offset-2">
        <h2> Verifikasi </h2>
      </div>
    </div>

    <div class="row text-center space-pad">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered table-hover table-striped display dttbl">
            <thead>
              <tr>
                <th> No. </th>
                <th> Pelapor </th>
                <th> Laporan </th>
                <th> Tanggal </th>
                <th> Status </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $kueri = "SELECT * from pengaduan INNER JOIN user ON pengaduan.id_user = user.id_user order by pengaduan.waktu DESC";
              $a = mysqli_query($konek,$kueri);
              $n = 0;
              while ($data = mysqli_fetch_array($a)) {
                $n++;
                $nm_lengkap = $data['nm_lengkap'];
                ?>
                <tr>
                  <td><?php echo $n; ?></td>
                  <td><?php echo $nm_lengkap ?></td>
                  <td class="more">
                    <?php 
                    $laporan = strip_tags($data['keterangan']);
                    echo $laporan = (strlen($laporan)>100) ? substr($laporan, 0, 100).'...' : $laporan ;
                    ?>
                  </td>
                  <td><?php echo $data['waktu']; ?></td>
                  <td><?php echo $status = ($data['status']=='1') ? "<button class='btn btn-info'>published</button>" : "<button class='btn btn-danger'>unpublished</butt" ;?></td>
                  <td> 
                    <?php echo "<a href='template.php?id=$data[0]'><button type='button' class='btn btn-success'> <i class='fa fa-eye'></i> Lihat </button></a>"; ?>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--End Dashboard Section-->
