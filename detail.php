<?php
$id = $_GET['id'];
$kueri = "SELECT * from pengaduan INNER JOIN user ON pengaduan.id_user = user.id_user and id_pengaduan = '$id' order by pengaduan.waktu DESC";
$a = mysqli_query($konek,$kueri);
$data = mysqli_fetch_array($a);

if ($data[7] == '1') {                             
  $caption = '<i class="fa fa-times"></i> Un-Verify';
  $tipe = 'btn-danger';
} else {
  $caption = '<i class="fa fa-check"></i> Verify';
  $tipe = 'btn-success';
}

?>
<!--Detail Section-->
<section  id="detail">
  <div class="container">
    <div class="row text-center">
      <div class="col-md-8 col-md-offset-2">
        <h2> Detail Laporan </h2>
      </div>
    </div>

    <div class="row space-pad">
      <div class="col-md-10 col-md-offset-1 container">

        <div class="row">
          <div class="col-md-2 col-sm-2"><i class="glyphicon glyphicon-pencil"></i> Laporan:</div>
          <div class="col-md-10 col-sm-10"><?php echo $data['keterangan']; ?></div>
        </div><hr>

        <div class="row">
          <div class="col-md-2 col-sm-2"><i class="fa fa-user"></i> Pelapor:</div>
          <div class="col-md-10 col-sm-10"><?php echo $data['nm_lengkap']; ?></div>
        </div><hr>

        <div class="row">
          <div class="col-md-2 col-sm-2"> <i class="fa fa-map-marker"></i> Lokasi:</div>
          <div class="col-md-10 col-sm-10"><?php echo $data['alamat']; ?></div>
        </div><hr>

        <div class="row">
          <div class="col-md-2 col-sm-2"> <i class="fa fa-clock-o"></i> Tanggal:</div>
          <div class="col-md-10 col-sm-10"><?php echo $data['waktu']; ?></div>
        </div><hr>

        <div class="row">
          <div class="col-md-2 col-sm-2"> <i class="fa fa-paperclip"></i> Lampiran:</div>
          <div class="col-md-10 col-sm-10">
            <div id="preview">
             <?php
             $foto = $data[5];
             if ($foto) {
               $arr = explode (", ", $foto);
               foreach ($arr as $key => $value) {
                ?>
                <img src='uploads/<?php echo $value; ?>' />
                <?php
              }
            } else { 
              echo "Tidak disertai lampiran."; 
            }
            ?>
          </div>
        </div>
      </div>

      <div class="group-detail">
        <?php 
        echo "<a href='autentifikasi.php?id=$data[0]&status=$data[7]'><button type='button' class='btn $tipe'>$caption</button></a>";
        ?>
        <a href='autentifikasi.php'>
          <button type='button' class='btn btn-info'>
            <i class="fa fa-arrow-left"></i> Kembali 
          </button>
        </a> 

      </div>
    </div>
  </div>
</section>
<!--End Detail Section-->
