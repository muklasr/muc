<?php
$id = $_SESSION['id'];
$query = mysqli_query($konek, "SELECT * from user where id_user = '$id'");
$data = mysqli_fetch_array($query);
?>
<!--Detail Section-->
<section  id="detail">
  <div class="container">
    <div class="row text-center">
      <div class="col-md-8 col-md-offset-2">
        <h2> Profil </h2>
      </div>
    </div>

    <div class="row text-center space-pad">
      <div class="col-md-10 col-md-offset-1">
        <div class="container" style="text-align: left;">
          <form role="form" id="form-profil">
            <div class="row">
              <div class="col-md-2 col-sm-2"> Username:</div>
              <div class="col-md-8 col-sm-8">
                <input type="text" id="p-uname" class="form-control" value='<?php echo $data['username']; ?>' readonly>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2 col-sm-2"> Password:</div>
              <div class="col-md-8 col-sm-8">
                <div class="input-group" id="cb">
                <input type='password' placeholder='Password lama' id='p-pw2' class='form-control' autocomplete="off" style="display: inline;">
                <span class='input-group-addon mata' id='basic-addon3'>
                    <a href='#' class='password-toggle3 form__toggle'>
                      <i class='fa fa-eye form__toggle-button'></i>
                    </a>
                  </span>
                </div>
                <div id="pesan"></div>
                <div class="input-group" id="ct">
                  <input type="password" id="p-pw" class="form-control" placeholder="Password baru" autocomplete="off" required>
                  <span class='input-group-addon mata' id='basic-addon3'>
                    <a href='#' class='password-toggle2 form__toggle'>
                      <i class='fa fa-eye form__toggle-button'></i>
                    </a>
                  </span>
                </div>
                <button type='button' class='btn btn-info' id="upass">
                  <i class="glyphicon glyphicon-pencil"></i> Ubah Password 
                </button>
                <br>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2 col-sm-2"> Nama Lengkap:</div>
              <div class="col-md-8 col-sm-8">
                <input type="text" id="p-nm" class="form-control" value='<?php echo $data['nm_lengkap']; ?>'>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2 col-sm-2">  No. Identitas:</div>
              <div class="col-md-8 col-sm-8">
                <input type="text" id="p-noid" class="form-control" value='<?php echo $data['no_id']; ?>'>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2 col-sm-2"> No. Telepon:</div>
              <div class="col-md-8 col-sm-8">
                <input type="text" id="p-notelp" class="form-control" value='<?php echo $data['no_telp']; ?>'>
                <button type='button' class='btn btn-success' id="tmbl-smpn" data-id='<?php echo $data[0]; ?>'><i class="fa fa-check"></i> Simpan </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--End Detail Section-->
