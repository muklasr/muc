<!--User Section-->
<section  id="user">
  <div class="container">
    <div class="row text-center">
      <div class="col-md-8 col-md-offset-2">
        <h2> Manajemen User </h2>
      </div>
    </div>
    <div class="row text-center space-pad">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered table-hover table-striped display dttbl" >
            <thead>
              <tr>
                <th> No. </th>
                <th> Username </th>
                <th> Akses </th>
                <th> Nama Lengkap </th>
                <th> No. Identitas </th>
                <th> No. Telepon </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $kueri = "SELECT * from user";
              $a = mysqli_query($konek,$kueri);
              $n = 0;
              while ($data = mysqli_fetch_array($a)) {
                $n = $n + 1;
                ?>
                <tr>
                  <td><?php echo $n; ?></td>
                  <td><?php echo $data['username']; ?></td>
                  <td><?php echo $data['akses']; ?></td>
                  <td><?php echo $data['nm_lengkap']; ?></td>
                  <td><?php echo $data['no_id']; ?></td>
                  <td><?php echo $data['no_telp']; ?></td>
                  <td> 
                    <?php echo "
                    <button type='button' class='btn btn-danger btn-sm tmbl-hps' title='Hapus' data-id='$data[0]'><i class='glyphicon glyphicon-trash'></i></button>
                    <button type='button' class='btn btn-success btn-sm tmbl-edit' title='Edit' data-id='$data[0]'> <i class='glyphicon glyphicon-pencil'></i></button>"; ?>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--End User Section-->
  
  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit </h4>
        </div>
        <div class="modal-body">
          <form role="form" id="form-update">
            <input type="text" id="mr-id" hidden>
            <div class="form-group">
              <label for="recipient-name" class="control-label">Username</label>
              <input type="text" id="mr-username" class="form-control" id="recipient-name">
            </div>
            <div class="form-group" hidden>
              <label for="recipient-name" class="control-label">Password</label>
              <input type="password" class="form-control mr-password" id="recipient-name">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="control-label">Akses</label>
              <input type="text" id="mr-akses" class="form-control" id="recipient-name">
            </div>
            
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <input type="submit" class="btn btn-primary btn-siper" name="save" value="Simpan perubahan">
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="confirmLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"> Konfirmasi </h4>
        </div>
        <div class="modal-body">
        <p>Data <span id="p-user"></span> akan dihapus, apakah anda yakin?</p>
        <input type="text" id="p-id" hidden>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary tmbl-ya"> Ya </button>
          <button type="button" class="btn btn-default" data-dismiss="modal"> Tidak </button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->