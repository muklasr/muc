<header>
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="" title="Media Lapor / Curhat dan Suara Rakyat"><span class="fa fa-comments"></span> <?php echo $appname ?> </a>
      </div>
      <!-- Collect the nav links for toggling -->
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav navbar-right">
          <?php 
          if (isset($_SESSION['akses'])) {
            if ($_SESSION['akses'] == '0') {

              echo "
              <li><a href='template.php#form'>FORM</a>
              </li>
              <li><a href='template.php#timeline'>BERANDA</a>
              </li>";
              ?>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['username'] ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="?menu=profil">Ubah Profil </a></li>
                  <li><a href="logout.php"> Keluar </a></li>
                </ul>
              </li>
              <?php
            } else if ($_SESSION['akses'] == '1') {
              echo "
              <li><a href='?menu=user'>USER</a>
              </li>
              <li><a href='?menu=autentifikasi'>VERIFIKASI</a>
                <li><a href='logout.php'>KELUAR</a>";
                } 

              } else {
                echo "
                <li><a href='#home'>MASUK</a>
                </li>
                <li><a href='#daftar'>DAFTAR</a>
                </li>";
              }
              ?>
            </li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container -->
    </nav>
  </header>
  <!--End Navigation -->  