<?php
include 'config.php';
if(isset($_SESSION['id'])){
  header('location: template.php');
}
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <title><?php echo $appname ?></title>
        <link rel="icon" type="image/png" href="assets/img/ico.png">
        <!--REQUIRED STYLE SHEETS-->
        <!-- BOOTSTRAP CORE STYLE CSS -->
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FONTAWESOME STYLE CSS -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLE CSS -->
        <link href="assets/css/style.css" rel="stylesheet" />
        <link href="assets/css/mr.css" rel="stylesheet" />
        <!-- GOOGLE FONT -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
     <?php 
     include 'nav.php';
     ?>

     <!--Header section  -->
     <div id="home">
      <div class="container" >
        <div class="row ">
          <div class="col-md-9 col-sm-9">
            <h3 class="head-main"><?php echo $appname ?></h3>
          </div>
          <div class="col-md-3 col-sm-3 form-login">
           <div class="div-trans text-center">
             <h3>Masuk </h3>
             <form role="form" id="form-login" >
              <div class="col-md-12 col-sm-12">
                <div class="form-group">
                  <input type="text" id="uname" name="uname" class="form-control log" required placeholder="Username">
                </div>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="form-group">
                  <div class="input-group">
                    <input type="password" id="pw" name="pw" class="form-control log pw" required placeholder="Password">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <input type="submit" name="kirim" class="btn btn-success" value="Masuk"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--End Header section  -->
<!--Daftar Section-->
<section  id="daftar">
  <div class="container">
    <div class="row text-center">
      <div class="col-md-8 col-md-offset-2">
        <h2> Daftar </h2>
      </div>
    </div>

    <div class="row text-center space-pad">
      <div class="col-md-8 col-md-offset-2">
        <form method="POST" action="daftar.php">
          Username          <input type="text" name="uname" class="form-control" required />
          Password          <br><div class="input-group"><input type="password" name="pw" class="form-control pw" required/></div><br>
          Nama Lengkap      <br><input type="text" name="nm_lengkap" class="form-control" required/><br>
          No. Identitas     <br><input type="text" name="no_id" class="form-control" required/><br>
          No. Telepon       <br><input type="text" name="no_telp" class="form-control" required/><br>
          <a href="#home"> Masuk </a>
          <input type="submit" name="daftar" class="btn btn-success" value="Daftar"/>
        </form>
      </div>
    </div>
  </div>
</section>
<!--End Daftar Section-->

<!-- Contact Section -->
<section  id="contact-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="social-icon">
          <strong> Alamat:</strong> 127.0.0.1/<?php echo $appname ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!--End Contact Section -->
<!--footer Section -->
<div class="for-full-back " id="footer">
  2017 <?php echo $appname ?> | All Right Reserved
</div>
<!--End footer Section -->
<!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
<!-- CORE JQUERY  -->
<script src="assets/plugins/jquery-3.2.1.js"></script>
<!-- BOOTSTRAP CORE SCRIPT   -->
<script src="assets/plugins/bootstrap.js"></script>
<!-- PARALLAX SCRIPT   -->
<script src="assets/plugins/4jquery.parallax-1.1.3.js"></script>
<!-- CUSTOM SCRIPT   -->
<script src="assets/js/custom.js"></script>
<script src="assets/js/mr.js"></script>

<script type="text/javascript"> 
  $(document).ready(function() {
    $('#form-login').submit(function( event) {
      event.preventDefault();
      $.ajax({
        url: 'login.php',
        type: 'POST',
        data: {
          uname: $("#uname").val(),
          pw: $("#pw").val()
        },
        success: function(response) {
          window.location.replace('template.php');
        }
      })
      
    });
  });
</script>
</body>
</html>
