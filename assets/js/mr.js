 //MATA
 $( function() {
  // Cache our jquery elements
  var $field = $(".pw");
  var _template = "<span class='input-group-addon mata' id='basic-addon4'><a href='#' class='password-toggle form__toggle'><i class='fa fa-eye form__toggle-button'></i></a></span>";
  
  // Inject the toggle button into the page
  $field.after( _template );
  
  // Cache the toggle button
  var $toggle = $(".password-toggle");
  
  // Toggle the field type
  $toggle.on("click", function(e) {
    e && e.preventDefault();
    if ($field.attr("type") == "password") {
      $field.attr("type","text");
      $(e.target).removeClass("fa-eye").addClass("fa-eye-slash");
    } else {
      $(e.target).removeClass("fa-eye-slash").addClass("fa-eye");
      $field.attr("type","password");
    }
  });

  // Password Lama
  var $field1 = $("#p-pw2");
  // Cache the toggle button
  var $toggle3 = $(".password-toggle3");
  
  // Toggle the field type
  $toggle3.on("click", function(e) {
    e && e.preventDefault();
    if ($field1.attr("type") == "password") {
      $field1.attr("type","text");
      $(e.target).removeClass("fa-eye").addClass("fa-eye-slash");
    } else {
      $(e.target).removeClass("fa-eye-slash").addClass("fa-eye");
      $field1.attr("type","password");
    }
  });
//Password Baru
  var $field2 = $("#p-pw");
  // Cache the toggle button
  var $toggle2 = $(".password-toggle2");
  
  // Toggle the field type
  $toggle2.on("click", function(e) {
    e && e.preventDefault();
    if ($field2.attr("type") == "password") {
      $field2.attr("type","text");
      $(e.target).removeClass("fa-eye").addClass("fa-eye-slash");
    } else {
      $(e.target).removeClass("fa-eye-slash").addClass("fa-eye");
      $field2.attr("type","password");
    }
  });
})

// UPDATE PROFIL
$(document).ready(function() {
  $('#ct').hide();
  $('#cb').hide();
});

$('#upass').on('click', function() {
  $(this).hide();
  $('#cb').show();
  $('#basic-addon4').show();
});

$(document).ready(function(){
  $("#p-pw2").keyup( function postinput(){
        var matchvalue = $(this).val(); // this.value
        $.ajax({ 
          url: 'eprofil.php',
          data: { matchvalue: matchvalue },
          type: 'post'
        }).done(function(response) {
          if (response == 'success') {
            $('#ct').show();
            $('#pesan').html('<span class="text-success"><i class="fa fa-check"></i> Cocok</span>');
          } else {
            $('#pesan').html('<span class="text-danger"><i class="fa fa-times"></i> Tidak cocok</span>');
            $('#ct').hide();
          }
        });
      });
}); 

$('#tmbl-smpn').on('click', function() {
  var id         = $(this).data('id');
  var username   = $('#p-uname').val();
  var pw         = $('#p-pw').val();
  var nm_lengkap = $('#p-nm').val();
  var noid       = $('#p-noid').val();
  var notelp     = $('#p-notelp').val();
  console.log(username);
  $.ajax({
    url: 'eprofil.php?id=' + id + '&username=' + username + '&pw=' + pw + '&nm_lengkap=' + nm_lengkap + '&noid=' + noid + '&notelp=' + notelp,
    complete : function(data) {
      alert('Profil berhasil diubah!');
      setTimeout(function() { location.reload(); }, 1000);
    }
  })
});


// IMAGES PREVIEW
$(function() {
  var imagesPreview = function(input, placeToInsertImagePreview) {
    if (input.files) {
      var filesAmount = input.files.length;
      for (i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = function(event) {
          $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
        }
        reader.readAsDataURL(input.files[i]);
      }
    }
  };
  $('#my_file').on('change', function() {
    imagesPreview(this, '#preview');
  });
});

//DATATABLE
$(document).ready( function () {
  $('.dttbl').DataTable();
} );

// UPDATE
$(".tmbl-edit").on("click", function() {
  var id = $(this).data("id");
  console.log(id);
  $.ajax({
    url: 'update.php?id='+id,
    dataType:"json", 
    success : function (data) {
      console.log(data.username);
      $('#mr-id').val(data.id);
      $('#mr-username').val(data.username);
      $('.mr-password').val(data.password);
      $('#mr-akses').val(data.akses);
    }
  })
  $('#myModal').modal('show');
  return false;
});

$('.btn-siper').on('click', function() {
  var id = $('#mr-id').val();
  var uname = $("#mr-username").val();
  var pw = $(".mr-password").val();
  var akses = $("#mr-akses").val();
  $.ajax({
    url: 'update.php?id=' + id + '&uname=' + uname + '&pw=' + pw + '&akses=' + akses,
    complete : function(data) {
      setTimeout(function() { location.reload(); }, 1000);
    }
  })
});

 // HAPUS
 $(".tmbl-hps").on("click", function() {
  var id = $(this).data("id");
  console.log(id);
  $.ajax({
    url: 'update.php?id='+id,
    dataType:"json", 
    success : function (data) {
      console.log(data.username);
      $('#p-user').html(data.username);
      $('#p-id').val(data.id);
    }
  })
  $('#modal-confirm').modal('show');
  return false;
});

 $('.tmbl-ya').on('click', function() {
   var id = $('#p-id').val();
   $.ajax({
     url: 'hapus.php?id='+id,
     complete : function(data) {
       setTimeout(function() { location.reload(); }, 1000);
     }
   })     
   $('#modal-confirm').modal('hide');
   return false;   
 });


 $(document).ready(function() {
    // Configure/customize these variables.
    var maxChar = 300;  
    var moretext = "Selengkapnya";
    var lesstext = "Ciutkan";
    

    $('.more').each(function() {
      var laporan = $(this).html();
      
      if(laporan.length > maxChar) {
       
        var awal = laporan.substr(0, maxChar);
        var sisa = laporan.substr(maxChar, laporan.length - maxChar);
        
        var html = awal + '<span class="moreellipses">...&nbsp;</span><span class="morecontent"><span>' + sisa + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
        
        $(this).html(html);
      }
      
    });
    
    $(".morelink").click(function(){
      if($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
      } else {
        $(this).addClass("less");
        $(this).html(lesstext);
      }
      $(this).parent().prev().toggle();
      $(this).prev().toggle();
      return false;
    });
  });