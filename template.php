<?php
include 'config.php';
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <title><?php echo $appname ?></title>
        <link rel="icon" type="image/png" href="assets/img/ico.png">
        <!--REQUIRED STYLE SHEETS-->
        <!-- BOOTSTRAP CORE STYLE CSS -->
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FONTAWESOME STYLE CSS -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLE CSS -->
        <link href="assets/css/mr.css" rel="stylesheet" />
        <link href="assets/css/style.css" rel="stylesheet" />
        <!-- GOOGLE FONT -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <!-- DATATABLES -->
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
      <?php
      include 'nav.php';

      if ($_GET['id']) {
        include 'detail.php';
      } else {
        if ($_SESSION['akses'] == '1') {

          if ($_GET['menu'] == 'user') {
            include 'user.php';
          } else if ($_GET['menu'] == 'autentifikasi') {
            include 'dashboard.php';
          } else {
            include 'dashboard.php';
          }

        } else if ($_SESSION['akses'] == '0') {
          if ($_GET['menu'] == 'profil') {
            include 'profil.php';
          } else {
            include 'konten.php';
          }
        } else {
          header('location:index.php');
        }
      }
      ?>
      <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
      <!-- CORE JQUERY  -->
      <script src="assets/plugins/jquery-3.2.1.js"></script>
      <!-- BOOTSTRAP CORE SCRIPT   -->
      <script src="assets/plugins/bootstrap.js"></script>
      <!-- CUSTOM SCRIPT   -->
      <script src="assets/js/custom.js"></script>
      <script src="assets/js/mr.js"></script>
    <!-- DATATABLES -->
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
    <!-- TinyMCE -->
    <script src="assets/plugins/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
  </body>
  </html>